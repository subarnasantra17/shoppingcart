//
//  ShoppingCartViewControllerTests.swift
//  ShoppingCartTests
//
//  Created by Subarna Santra on 4/29/18.
//  Copyright © 2018 Subarna. All rights reserved.
//

@testable import ShoppingCart
import XCTest

class ShoppingCartViewControllerTests: XCTestCase {
    
    var shoppingCartViewController: ShoppingCartViewController!
    var window: UIWindow!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        window = UIWindow()
        self.setupShoppingListViewController()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        window = nil
        super.tearDown()
    }
    
    func setupShoppingListViewController() {
        let bundle = Bundle.main
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        shoppingCartViewController = storyboard.instantiateViewController(withIdentifier: "ShoppingCartViewController") as! ShoppingCartViewController
        
    }
    
    func loadShoppingListViewControllerView() {
        window.addSubview(shoppingCartViewController.view)
        RunLoop.current.run(until: Date())
    }
    
    
    //MARK: CREATE SHOPPING CART SUBSTITUE CLASS
    
    class ShoppingCartInteractorSub: ShoppingCartViewControllerProtocol {
        
        // MARK: Method call expectations
        
        var fetchShoppingListCalled = false
        
        // MARK: Spied methods
        
        func fetchShoppingList(completionHandler: @escaping (ErrorTypes) -> Void) {
            
            fetchShoppingListCalled = true
            completionHandler(ErrorTypes.NO_ERROR)
        }
        
        func getTotalItemCount()->Int {
            return 1
        }
        
        func getItemAtIndex(index : Int) -> DisplayProduct? {
            return DisplayProduct(type: .APPLE, productName: "Apple", price: "0.5")
        }
        
        func displayTotalAmount() -> String {
            
            return "total amount"
            
        }
        
        func displayTotalSavingsAmount() -> String {
            return "total savings amount"
        }
    }
    
    
    //MARK: - START TESTING
    
    //test ShoppingCartInteractorSub->fetchShoppingList is called when the shopping cart view appears
    func testFetchShoppingListCalledWhenViewDidAppear() {
        
        // Given
        let shoppingCartInteractorSub = ShoppingCartInteractorSub()
        shoppingCartViewController.shoppingCartInteractor = shoppingCartInteractorSub
        self.loadShoppingListViewControllerView()
        
        // When
        shoppingCartViewController.viewDidAppear(true)
        
        // Then
        XCTAssert(shoppingCartInteractorSub.fetchShoppingListCalled, "Should fetch shopping list right after the view appears")
    }
    
    
    func testNumberOfRowsShouldEqaulToGetTotalItemCount() {
        
        let shoppingCartInteractorSub = ShoppingCartInteractorSub()
        shoppingCartViewController.shoppingCartInteractor = shoppingCartInteractorSub
        self.loadShoppingListViewControllerView()
        
        shoppingCartViewController.viewDidAppear(true)
        
        // Given
        let numberOfRowsInSection0 = shoppingCartViewController.shoppingListTableView.numberOfRows(inSection: 0)
        
        // Then
        XCTAssertEqual(numberOfRowsInSection0, shoppingCartInteractorSub.getTotalItemCount(), "The number of table view rows should equal the number of total item count")
    }
    
    
    func testGetItemAtIndex() {
        
        let shoppingCartInteractorSub = ShoppingCartInteractorSub()
        shoppingCartViewController.shoppingCartInteractor = shoppingCartInteractorSub
        self.loadShoppingListViewControllerView()
        
        shoppingCartViewController.viewDidAppear(true)
        
        //Get the first item of the first row from the display list
        let firstDisplayItem = shoppingCartInteractorSub.getItemAtIndex(index: 0)
        
        // Get the first row of the first section of the shopping list
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = shoppingCartViewController.shoppingListTableView.cellForRow(at: indexPath) as! ShoppingListTableViewCell
        
        // Then
        XCTAssertEqual(cell.productNameLabel?.text, firstDisplayItem?.productName, "The first row of the shopping list table should show the product name of the item from ShoppingListInteractor->getItemAtIndex(index:0)")
        
        XCTAssertEqual(cell.productPriceLabel?.text, firstDisplayItem?.price, "The first row of the shopping list table should show the product price of the item from ShoppingListInteractor->getItemAtIndex(index:0)")
        
    }
    
    func testDisplayTotalAmount() {
        
        let shoppingCartInteractorSub = ShoppingCartInteractorSub()
        shoppingCartViewController.shoppingCartInteractor = shoppingCartInteractorSub
        self.loadShoppingListViewControllerView()
        
        shoppingCartViewController.viewDidAppear(true)
        
        //Get the total amoount
        let totalDisplayAmount = shoppingCartInteractorSub.displayTotalAmount()
        
        // Then
        XCTAssertEqual(shoppingCartViewController.totalAmountLabel?.text, totalDisplayAmount, "The display amount should be equal to ShoppingCartInteractor->displayTotalAmount()")
        
    }
    
}
