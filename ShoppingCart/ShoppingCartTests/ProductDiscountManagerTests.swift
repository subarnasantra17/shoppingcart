//
//  ProductDiscountManagerTests.swift
//  ShoppingCartTests
//
//  Created by Subarna Santra on 4/29/18.
//  Copyright © 2018 Subarna. All rights reserved.
//

@testable import ShoppingCart
import XCTest

class ProductDiscountManagerTests: XCTestCase {
    
    lazy var productDiscountManager: ProductDiscountManager = ProductDiscountManager()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    //MARK: - APPLY THE DISCOUNT ON LIST OF APPLES
    func testApplyDiscountRateToApples() {
        
        var shoppingList : [ShoppedProduct] = []
        shoppingList.append(ShoppedProduct(type: .APPLE, productName: .APPLE, price: .APPLE, discountedAmount: 0.0))
        shoppingList.append(ShoppedProduct(type: .ORANGE, productName: .ORANGE, price: ProductPrices.ORANGE, discountedAmount: 0.0))
        shoppingList.append(ShoppedProduct(type: .APPLE, productName: .APPLE, price: ProductPrices.APPLE, discountedAmount: 0.0))
        
        let modShoppingList = productDiscountManager.applyDiscountRateToApples(productList: shoppingList)
        
        
        //Get the first apple, the first item from the list
        let apple1 = modShoppingList[0]
        
        
        // Assert the second apple is free
        XCTAssertEqual(apple1.discountedAmount, 0.0, "The first apple does not have any discount")
        XCTAssertEqual(apple1.finalAmount, ProductPrices.APPLE.rawValue, "The second apple is NOT free")
        
        //Get the seccond apple, the third item from the list
        let apple2 = modShoppingList[2]
        
        
        // Assert the second apple is free
        XCTAssertEqual(apple2.discountedAmount, ProductPrices.APPLE.rawValue, "The second apple has full discount")
        XCTAssertEqual(apple2.finalAmount, 0.0, "The second apple is now free")
    }
    
    //MARK: - APPLY THE DISCOUNT ON LIST OF ORANGES
    func testApplyDiscountRateToOranges() {
        
        var shoppingList : [ShoppedProduct] = []
        shoppingList.append(ShoppedProduct(type: .APPLE, productName: .APPLE, price: .APPLE, discountedAmount: 0.0))
        shoppingList.append(ShoppedProduct(type: .APPLE, productName: .APPLE, price: ProductPrices.APPLE, discountedAmount: 0.0))
        shoppingList.append(ShoppedProduct(type: .ORANGE, productName: .ORANGE, price: ProductPrices.ORANGE, discountedAmount: 0.0))
        shoppingList.append(ShoppedProduct(type: .ORANGE, productName: .ORANGE, price: ProductPrices.ORANGE, discountedAmount: 0.0))
        shoppingList.append(ShoppedProduct(type: .ORANGE, productName: .ORANGE, price: ProductPrices.ORANGE, discountedAmount: 0.0))
        shoppingList.append(ShoppedProduct(type: .ORANGE, productName: .ORANGE, price: ProductPrices.ORANGE, discountedAmount: 0.0))
        
        let modShoppingList = productDiscountManager.applyDiscountRateToOranges(productList: shoppingList)
        
        
        //Get the seccond orange, the fourth item from the list
        let orange2 = modShoppingList[3]
        
        
        // Assert the third orange is free
        XCTAssertEqual(orange2.discountedAmount, 0.0, "The second orange does not have any discount")
        XCTAssertEqual(orange2.finalAmount, ProductPrices.ORANGE.rawValue, "The second orange is NOT free")
        
        //Get the third apple, the fifth item from the list
        let orange3 = modShoppingList[4]
        
        
        // Assert the third orange is free
        XCTAssertEqual(orange3.discountedAmount, ProductPrices.ORANGE.rawValue, "The third orange has full discount")
        XCTAssertEqual(orange3.finalAmount, 0.0, "The third orange is now free")
        
        
        //Get the fourth orange, the sixth item from the list
        let orange4 = modShoppingList[5]
        
        
        // Assert the third orange is free
        XCTAssertEqual(orange4.discountedAmount, 0.0, "The fourth orange does not have any discount")
        XCTAssertEqual(orange4.finalAmount, ProductPrices.ORANGE.rawValue, "The fourth orange is NOT free")
    }
}

