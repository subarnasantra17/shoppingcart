//
//  ShoppingCartInteractorTests.swift
//  ShoppingCartTests
//
//  Created by Subarna Santra on 4/29/18.
//  Copyright © 2018 Subarna. All rights reserved.
//

@testable import ShoppingCart
import XCTest

class ShoppingCartInteractorTests: XCTestCase {
    
    lazy var shoppingCartInteractor: ShoppingCartInteractor = ShoppingCartInteractor()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    class ShoppingCartManagerSub: ShoppingCartManager {
        
        var fetchListCalled = false
        
        override func fetchShoppingList(completionHandler: @escaping ([ShoppedProduct], ErrorTypes) -> Void) {
            fetchListCalled = true
            completionHandler([], ErrorTypes.NO_ERROR)
        }
    }
    
    
    func testFetchShoppingListWasCalled() {
        
        //create shopping list manager substitue
        let shoppingCartManagerSub = ShoppingCartManagerSub(shoppingListStore: ShoppingCartAPI())
        shoppingCartInteractor.myShoppingCartManager = shoppingCartManagerSub
        
        
        // create expectation for the asynchronous ShoppingListManager->FetchShoppingList Call
        let expect = expectation(description: "Wait for fetchShoppingList() to return")
        shoppingCartInteractor.fetchShoppingList { (error) in
            
            expect.fulfill()
        }
        waitForExpectations(timeout: 1.1)
        
        // Assert ShoppingListManager->fetchShoppingList was called
        XCTAssert(shoppingCartManagerSub.fetchListCalled, "ShoppingListManager->FetchShoppingList was called")
    }
    
    func testGetTotalItemCount() {
        
        var shoppingList : [ShoppedProduct] = []
        shoppingList.append(ShoppedProduct(type: .APPLE, productName: .APPLE, price: .APPLE, discountedAmount: 0.0))
        shoppingList.append(ShoppedProduct(type: .ORANGE, productName: .ORANGE, price: ProductPrices.ORANGE, discountedAmount: 0.0))
        shoppingList.append(ShoppedProduct(type: .APPLE, productName: .APPLE, price: ProductPrices.APPLE, discountedAmount: 0.0))
        
        shoppingCartInteractor.createShoppingList(shoppingList: shoppingList)
        
        // Assert ShopListInteractor->getTotalAmount was called
        XCTAssertEqual(shoppingCartInteractor.getTotalItemCount(), 3, "Total number of items in the list is 3")
        
    }
    
    func testGetTotalAmount() {
        
        var shoppingList : [ShoppedProduct] = []
        shoppingList.append(ShoppedProduct(type: .APPLE, productName: .APPLE, price: .APPLE, discountedAmount: 0.0))
        shoppingList.append(ShoppedProduct(type: .ORANGE, productName: .ORANGE, price: ProductPrices.ORANGE, discountedAmount: 0.0))
        shoppingList.append(ShoppedProduct(type: .APPLE, productName: .APPLE, price: ProductPrices.APPLE, discountedAmount: 0.0))
        
        shoppingCartInteractor.createShoppingList(shoppingList: shoppingList)
        
        // Assert ShopListInteractor->getTotalAmount was called
        XCTAssertEqual(shoppingCartInteractor.getTotalAmount(), 1.1, "Total Amount is the total of all the actual product prices AFTER applying discount")
    }
    
    func testGetActualTotalAmountt() {
        
        var shoppingList : [ShoppedProduct] = []
        shoppingList.append(ShoppedProduct(type: .APPLE, productName: .APPLE, price: .APPLE, discountedAmount: 0.0))
        shoppingList.append(ShoppedProduct(type: .ORANGE, productName: .ORANGE, price: ProductPrices.ORANGE, discountedAmount: 0.0))
        shoppingList.append(ShoppedProduct(type: .APPLE, productName: .APPLE, price: ProductPrices.APPLE, discountedAmount: 0.0))
        
        shoppingCartInteractor.setShoppingList(shoppingList: shoppingList)
        
        // Assert ShopListInteractor->getActualTotalAmount was called
        XCTAssertEqual(shoppingCartInteractor.getActualTotalAmount(), 1.6, "Total Amount is the total of all the actual product prices without discount")
    }
    
    func testGetTotalSaving() {
        
        var shoppingList : [ShoppedProduct] = []
        shoppingList.append(ShoppedProduct(type: .APPLE, productName: .APPLE, price: .APPLE, discountedAmount: 0.0))
        shoppingList.append(ShoppedProduct(type: .ORANGE, productName: .ORANGE, price: ProductPrices.ORANGE, discountedAmount: 0.0))
        shoppingList.append(ShoppedProduct(type: .APPLE, productName: .APPLE, price: ProductPrices.APPLE, discountedAmount: 0.0))
        
        shoppingCartInteractor.createShoppingList(shoppingList: shoppingList)
        
        // Assert ShopListInteractor->getTotalSaving was called
        XCTAssertEqual(shoppingCartInteractor.getTotalSaving(), 0.5, "Total savings is the sum of the discounted pricess")
    }
    
}
