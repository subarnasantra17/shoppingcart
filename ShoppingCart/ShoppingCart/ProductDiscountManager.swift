//
//  ProductDiscountManager.swift
//  ShoppingCart
//
//  Created by Subarna Santra on 4/29/18.
//  Copyright © 2018 Subarna. All rights reserved.
//

import Foundation

protocol ProductDiscountManagerProtocol {
    
    func applyDiscount(shoppingList:[ShoppedProduct]) -> [ShoppedProduct]
}

class ProductDiscountManager:ProductDiscountManagerProtocol {
    
    func applyDiscount(shoppingList:[ShoppedProduct]) -> [ShoppedProduct] {
        
        var discountedShoppingList = shoppingList
        
        if (discountedShoppingList.count > 0) {
            
            discountedShoppingList = self.applyDiscountRateToApples(productList: shoppingList)
            
            discountedShoppingList = self.applyDiscountRateToOranges(productList: discountedShoppingList)
        }
        
        return discountedShoppingList
    }
    
    
    func applyDiscountRateToApples(productList:[ShoppedProduct]) -> [ShoppedProduct] {
        
        var discountedProductList = productList
        
        var paidApple = 0
        
        discountedProductList = productList.map({
            (product: ShoppedProduct) -> ShoppedProduct in
            
            var modProduct = product
            
            if (product.type == .APPLE) {
                
                paidApple = paidApple + 1
                
                if (paidApple == 2) {
                    modProduct.discountedAmount = ProductPrices.APPLE.rawValue
                    paidApple = 0
                }
            }
            return modProduct
        })
        
        return discountedProductList
    }
    
    func applyDiscountRateToOranges(productList:[ShoppedProduct]) -> [ShoppedProduct] {
        
        var discountedProductList = productList
        
        var paidOrange = 0
        
        discountedProductList = productList.map({
            (product: ShoppedProduct) -> ShoppedProduct in
            
            var modProduct = product
            
            if (product.type == .ORANGE) {
                paidOrange = paidOrange + 1
                
                if (paidOrange == 3) {
                    modProduct.discountedAmount = ProductPrices.ORANGE.rawValue
                    paidOrange = 0
                }
            }
            
            return modProduct
        })
        
        return discountedProductList
    }
    
    
}
