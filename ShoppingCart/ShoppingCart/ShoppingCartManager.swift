//
//  ShoppingCartManager.swift
//  ShoppingCart
//
//  Created by Subarna Santra on 4/29/18.
//  Copyright © 2018 Subarna. All rights reserved.
//

import Foundation

protocol ShoppingCartManagerProtocol {
    
    func fetchShoppingList(completionHandler: @escaping ([ShoppedProduct], ErrorTypes) -> Void)
}

class ShoppingCartManager:ShoppingCartManagerProtocol {
    
    var myShoppingListStore: ShoppingCartStoreProtocol
    
    init(shoppingListStore: ShoppingCartStoreProtocol) {
        self.myShoppingListStore = shoppingListStore
    }
    
    
    func fetchShoppingList(completionHandler: @escaping ([ShoppedProduct], ErrorTypes) -> Void) {
        
        self.myShoppingListStore.fetchShoppingList { (shoppingList, error) in
            
            completionHandler(shoppingList, error)
        }
    }
}
