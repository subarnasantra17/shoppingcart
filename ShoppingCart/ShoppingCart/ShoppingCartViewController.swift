//
//  ShoppingCartViewController.swift
//  ShoppingCart
//
//  Created by Subarna Santra on 4/29/18.
//  Copyright © 2018 Subarna. All rights reserved.
//

import UIKit

protocol ShoppingCartViewControllerProtocol {
    
    func fetchShoppingList(completionHandler: @escaping (ErrorTypes) -> Void)
    
    func getTotalItemCount()->Int
    
    func getItemAtIndex(index : Int) -> DisplayProduct?
    
    func displayTotalAmount() -> String
    
    func displayTotalSavingsAmount() -> String
}

let shoppingListTableViewCell = "ShoppingListTableViewCell"

class ShoppingCartViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var shoppingListTableView: UITableView!
    
    @IBOutlet weak var totalAmountLabel: UILabel!
    
    @IBOutlet weak var totalSavingsLabel: UILabel!
    
    lazy var shoppingCartInteractor: ShoppingCartViewControllerProtocol = ShoppingCartInteractor()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.setupTableViewList() //setup table view
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.showShoppingList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - SETUP SHOPPING LIST TABLE VIEW
    func setupTableViewList() {
        
        self.shoppingListTableView.register(UINib(nibName: shoppingListTableViewCell, bundle:nil), forCellReuseIdentifier: shoppingListTableViewCell)
        
        self.shoppingListTableView.delegate = self
        self.shoppingListTableView.dataSource = self
        
        self.shoppingListTableView.tableFooterView = UIView()
    }
    
    //MARK: - SHOW SHOPPING LIST
    func showShoppingList() {
        
        //fetch shopping list
        self.shoppingCartInteractor.fetchShoppingList { [weak self] (error) in
            
            if let strongSelf = self {
                
                if (error == ErrorTypes.NO_ERROR) {
                     strongSelf.shoppingListTableView.reloadData() //reload shopping list
                    strongSelf.showAmountDetails() //show the total amount
                }
            }
        }
    }
    
    //MARK: - SHOW TOTAL AMOUNT
    
    func showAmountDetails() {
        self.totalSavingsLabel.text = self.shoppingCartInteractor.displayTotalSavingsAmount()
        self.totalAmountLabel.text = self.shoppingCartInteractor.displayTotalAmount() //get the total amount as string
    }

}

// MARK: - Table view data source

extension ShoppingCartViewController
{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.shoppingCartInteractor.getTotalItemCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: shoppingListTableViewCell, for: indexPath) as! ShoppingListTableViewCell
        cell.setupCellData(item:self.shoppingCartInteractor.getItemAtIndex(index: indexPath.row))
        return cell
    }
}


