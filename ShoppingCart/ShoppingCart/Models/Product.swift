//
//  Product.swift
//  ShoppingCart
//
//  Created by Subarna Santra on 4/29/18.
//  Copyright © 2018 Subarna. All rights reserved.
//

import Foundation

enum ErrorTypes : Int {
    
    case NO_ERROR = 0
    case ERROR_FETCHING = 1
}

enum ProductTypes: String {
    
    case APPLE = "APPLE"
    case ORANGE = "ORANGE"
}

enum ProductNames: String {
    
    case APPLE = "Apple"
    case ORANGE = "Orange"
}

enum ProductPrices: Float {
    
    case APPLE = 0.6
    case ORANGE = 0.25
}

struct ShoppedProduct {
    
    var type: ProductTypes
    var productName: ProductNames
    var price : ProductPrices
    var discountedAmount : Float
    var finalAmount : Float {
        get {
            return (price.rawValue - discountedAmount)
        }
    }
}

struct DisplayProduct {
    
    var type: ProductTypes
    var productName: String
    var price : String
}
