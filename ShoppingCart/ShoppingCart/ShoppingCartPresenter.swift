//
//  ShoppingCartPresenter.swift
//  ShoppingCart
//
//  Created by Subarna Santra on 4/29/18.
//  Copyright © 2018 Subarna. All rights reserved.
//

import Foundation

protocol ShoppingCartPresentationProtocol {
    
    func presentShoppingList(shoppingList: [ShoppedProduct]) -> [DisplayProduct]
    func presentAmount(amount: Float) -> String
}

var currencyString = "£"

class ShoppingCartPresenter: ShoppingCartPresentationProtocol {
    
    func presentShoppingList(shoppingList: [ShoppedProduct]) -> [DisplayProduct] {
        
        let modShoppingList = shoppingList.map({
            (product: ShoppedProduct) -> DisplayProduct in
            
            let modProduct = DisplayProduct(type: product.type, productName: product.productName.rawValue, price: self.presentAmount(amount: product.finalAmount))
            
            return modProduct
        })
        
        return modShoppingList
    }
    
    func presentAmount(amount: Float) -> String {
        
        return currencyString + " " + String(format: "%.2f", amount)
    }
}
