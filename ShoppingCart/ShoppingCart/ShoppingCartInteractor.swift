//
//  ShoppingCartInteractor.swift
//  ShoppingCart
//
//  Created by Subarna Santra on 4/29/18.
//  Copyright © 2018 Subarna. All rights reserved.
//

import Foundation

class ShoppingCartInteractor: ShoppingCartViewControllerProtocol {
    
    private var myShoppingList : [ShoppedProduct] = []
    private var myDisplayShoppingList : [DisplayProduct] = []
    
    lazy var myShoppingCartManager: ShoppingCartManagerProtocol = ShoppingCartManager(shoppingListStore: ShoppingCartAPI())
    
    lazy var myShoppingCartPresenter : ShoppingCartPresentationProtocol  = ShoppingCartPresenter()
    
    lazy var myProductDiscountManager:ProductDiscountManagerProtocol = ProductDiscountManager()
    
    func fetchShoppingList(completionHandler: @escaping ( ErrorTypes) -> Void) {
        
        self.myShoppingCartManager.fetchShoppingList { [weak self] (shopplingList, error) in
            
            if let strongSelf = self {
                
                strongSelf.createShoppingList(shoppingList: shopplingList)
            }
            
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
    }
    
    func getShoppingList()->[ShoppedProduct] {
        
        return self.myShoppingList
    }
    
    func setShoppingList(shoppingList: [ShoppedProduct]) {
        
        self.myShoppingList = shoppingList
        
    }
    
    func createShoppingList(shoppingList: [ShoppedProduct]) {
        
        var modShoppingList =  shoppingList
        if (shoppingList.count > 0) {
            modShoppingList =  self.applyDiscountShoppingList(shoppingList: shoppingList)
        }
        self.setShoppingList(shoppingList: modShoppingList)
        self.setDisplayShoppingList(shoppingList: modShoppingList)
    }
    
    func applyDiscountShoppingList(shoppingList: [ShoppedProduct])-> [ShoppedProduct] {
        
        return self.myProductDiscountManager.applyDiscount(shoppingList: shoppingList)
    }
    
    
    func setDisplayShoppingList(shoppingList: [ShoppedProduct]) {
        
        self.myDisplayShoppingList = self.myShoppingCartPresenter.presentShoppingList(shoppingList: shoppingList)
    }
    
    
    func getTotalSaving() -> Float {
        
        var totalAmount: Float = 0.0
        
        if (self.myShoppingList.count > 0) {
            
            totalAmount = self.myShoppingList.reduce(totalAmount, { (totalAmount, product) -> Float in
                totalAmount + product.discountedAmount
            })
        }
        
        return totalAmount
    }
    
    func getActualTotalAmount() -> Float {
        
        var totalAmount: Float = 0.0
        
        if (self.myShoppingList.count > 0) {
            
            totalAmount = self.myShoppingList.reduce(totalAmount, { (totalAmount, product) -> Float in
                totalAmount + product.price.rawValue
            })
        }
        
        return totalAmount
    }
    
    func getTotalAmount() -> Float {
        
        var totalAmount: Float = 0.0
        
        if (self.myShoppingList.count > 0) {
            
            totalAmount = self.myShoppingList.reduce(totalAmount, { (totalAmount, product) -> Float in
                totalAmount + product.finalAmount
            })
        }
        
        return totalAmount
    }
    
    
    func displayTotalAmount() -> String {
        
        return self.myShoppingCartPresenter.presentAmount(amount: self.getTotalAmount())
    }
    
    func displayTotalSavingsAmount() -> String {
        
        return self.myShoppingCartPresenter.presentAmount(amount: self.getTotalSaving())
    }
    
    func getTotalItemCount()->Int {
        
        return self.myShoppingList.count
    }
    
    func getItemAtIndex(index : Int) -> DisplayProduct? {
        
        if (index >= 0 && index < self.myDisplayShoppingList.count) {
            
            return self.myDisplayShoppingList[index]
        }
        
        return nil
    }
    
}
