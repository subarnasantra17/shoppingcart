//
//  ShoppingListTableViewCell.swift
//  ShoppingCart
//
//  Created by Subarna Santra on 4/29/18.
//  Copyright © 2018 Subarna. All rights reserved.
//

import UIKit

class ShoppingListTableViewCell: UITableViewCell {

    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    
    var cellItem: DisplayProduct?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.clearCellData()
    }
    
    func clearCellData() {
        self.productNameLabel.text = ""
        self.productPriceLabel.text = ""
    }
    
    func setupCellData(item: DisplayProduct?) {
        
        self.cellItem = item //store the cell specific data for later use
        
        if let thisItem = self.cellItem {
            self.productPriceLabel.text = thisItem.price
            self.productNameLabel.text = thisItem.productName
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
