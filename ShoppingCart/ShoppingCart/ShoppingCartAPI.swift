//
//  ShoppingCartAPI.swift
//  ShoppingCart
//
//  Created by Subarna Santra on 4/29/18.
//  Copyright © 2018 Subarna. All rights reserved.
//

import Foundation

protocol ShoppingCartStoreProtocol {
    
    func fetchShoppingList(completionHandler: @escaping ([ShoppedProduct], ErrorTypes) -> Void)
}

class ShoppingCartAPI: ShoppingCartStoreProtocol {
    
    func fetchShoppingList(completionHandler: @escaping ([ShoppedProduct], ErrorTypes) -> Void) {
        
        var myShoppingList : [ShoppedProduct] = []
        myShoppingList.append(ShoppedProduct(type: .APPLE, productName: .APPLE, price: .APPLE, discountedAmount: 0.0))
        myShoppingList.append(ShoppedProduct(type: .ORANGE, productName: .ORANGE, price: ProductPrices.ORANGE, discountedAmount: 0.0))
        myShoppingList.append(ShoppedProduct(type: .APPLE, productName: .APPLE, price: ProductPrices.APPLE, discountedAmount: 0.0))
        myShoppingList.append(ShoppedProduct(type: .ORANGE, productName: .ORANGE, price: ProductPrices.ORANGE, discountedAmount: 0.0))
        myShoppingList.append(ShoppedProduct(type: .ORANGE, productName: .ORANGE, price: ProductPrices.ORANGE, discountedAmount: 0.0))
        myShoppingList.append(ShoppedProduct(type: .ORANGE, productName: .ORANGE, price: ProductPrices.ORANGE, discountedAmount: 0.0))
        
        completionHandler(myShoppingList, ErrorTypes.NO_ERROR)
    }
}
